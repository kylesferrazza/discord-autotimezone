package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
)

func main() {
	discordToken, tokenPresent := os.LookupEnv("DISCORD_TOKEN")
	if !tokenPresent {
		fmt.Println("DISCORD_TOKEN environment variable missing.")
		return
	}

	discord, err := discordgo.New("Bot " + discordToken)
	if err != nil {
		fmt.Println("Error starting Discord session:", err)
		return
	}

	discord.AddHandler(msgHandler)

	err = discord.Open()
	if err != nil {
		fmt.Println("Error opening Discord connection:", err)
		return
	}

	discord.UpdateListeningStatus("the flow of time")

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running. Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
	fmt.Println("\nSignal received. Exiting...")

	// Close the Discord session.
	discord.Close()
}
