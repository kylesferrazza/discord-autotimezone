module gitlab.com/kylesferrazza/discord-autotimezone

go 1.16

require (
	github.com/bwmarrin/discordgo v0.23.2
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/crypto v0.0.0-20211108221036-ceb1ce70b4fa // indirect
	golang.org/x/sys v0.0.0-20211113001501-0c823b97ae02 // indirect
)
