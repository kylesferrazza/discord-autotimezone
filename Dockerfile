FROM golang:1.17-alpine AS build
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY *.go ./
RUN go build -o /discord-autotimezone



FROM alpine:latest
WORKDIR /
COPY --from=build /discord-autotimezone /discord-autotimezone
ENTRYPOINT ["/discord-autotimezone"]
