package main

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

var tzoneRegex regexp.Regexp = *regexp.MustCompile("(^[0-9]+:[0-9]{2}[A|P]M)-([E|C|M|P])S?T")

var locations = map[byte]*time.Location{
	'E': time.FixedZone("Eastern ", -5*60*60),
	'C': time.FixedZone("Central ", -6*60*60),
	'M': time.FixedZone("Mountain", -7*60*60),
	'P': time.FixedZone("Pacific ", -8*60*60),
}

var locationOrder = []byte{'E', 'C', 'M', 'P'}

func formatMessage(fullmatch string, time time.Time) string {
	replyStr := ""
	replyStr += "Time (" + fullmatch + ") in all US zones:\n"
	replyStr += "```"
	for _, key := range locationOrder {
		location := locations[key]
		replyStr += location.String() + ": " + time.In(location).Format("03:04 PM") + "\n"
	}
	replyStr += "```"
	return replyStr
}

func msgHandler(sess *discordgo.Session, msg *discordgo.MessageCreate) {
	if msg.Author.Bot {
		return
	}
	fields := strings.Fields(msg.Content)
	for _, field := range fields {
		groups := tzoneRegex.FindStringSubmatch(field)
		if len(groups) == 0 {
			continue
		}
		timeStr := groups[1]
		zoneChar := groups[2][0]
		zone := locations[zoneChar]
		timeParsed, err := time.ParseInLocation("3:04PM", timeStr, zone)
		if err != nil {
			fmt.Println("error parsing")
			continue
		}
		replyContent := formatMessage(groups[0], timeParsed)
		sess.ChannelMessageSendReply(msg.ChannelID, replyContent, msg.Reference())
	}
}
