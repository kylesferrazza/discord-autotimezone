{
  description = "discord-autotimezone";

  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    defaultPackage.x86_64-linux = pkgs.buildGoModule {
      name = "discord-autotimezone";
      version = "0.1";

      src = ./.;
      vendorSha256 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    };
    devShell.x86_64-linux = pkgs.mkShell {
      name = "discord-autotimezone";
      buildInputs = with pkgs; [
        go
        gopls
        helmfile
        doppler
      ];
    };
  };
}
